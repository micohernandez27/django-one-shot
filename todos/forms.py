from django.forms import ModelForm
from .models import TodoList,TodoItem

class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            'name',
        ]
    def __str__(self):
        return "list"

class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            'task',
            'due_date',
            'is_completed',
            'list',
        ]
    def __str__(self):
        return "item"
